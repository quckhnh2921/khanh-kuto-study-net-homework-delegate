﻿using Delegate_Homework_2;

Calculate calculate = new Calculate();
Menu menu = new Menu();
int choice = 0;
do
{
    try
    {
        menu.MenuDisplay();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                var doubleResult = calculate.DoubleNumber();
                Console.WriteLine($"Result: {doubleResult}");
                break;
            case 2:
                var tripleResult = calculate.TripleNumber();
                Console.WriteLine($"Result: {tripleResult}");
                break;
            case 3:
                var squareResult = calculate.SquareNumber();
                Console.WriteLine($"Result: {squareResult}");
                break;
            case 4:
                var cubeResult = calculate.CubeNumber();
                Console.WriteLine($"Result: {cubeResult}");
                break;
            case 5:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 5");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);