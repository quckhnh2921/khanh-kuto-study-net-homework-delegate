﻿namespace Delegate_Homework_2
{
    public class Menu
    {
        public void MenuDisplay()
        {
            Console.WriteLine("============Menu============");
            Console.WriteLine("| 1. Double number         |");
            Console.WriteLine("| 2. Triple number         |");
            Console.WriteLine("| 3. Square number         |");
            Console.WriteLine("| 4. Cube number           |");
            Console.WriteLine("| 5. Exit                  |");
            Console.WriteLine("============================");
            Console.Write("Choose: ");
        }
    }
}
