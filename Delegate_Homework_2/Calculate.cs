﻿namespace Delegate_Homework_2
{
    public class Calculate : Delegate
    {
        public int DoubleNumber()
        {
            Custom opeation = new Custom(Double);
            int x = InputNumber();
            return PrintResult(x, opeation);
        }
        public int TripleNumber()
        {
            Custom operation = new Custom(Triple);
            int x = InputNumber();
            return PrintResult(x, operation);
        }
        public int SquareNumber()
        {
            Custom operation = new Custom(Square);
            int x = InputNumber();
            return PrintResult(x, operation);
        }
        public int CubeNumber()
        {
            Custom operation = new Custom(Cube);
            int x = InputNumber();
            return PrintResult(x, operation);
        }
        private int PrintResult(int x, Custom operation) => operation(x);
        private int Double(int x) => x * 2;
        private int Triple(int x) => x * 3;
        private int Square(int x) => x * x;
        private int Cube(int x) => x * x * x;
        private int InputNumber()
        {
            Console.WriteLine("Enter x: ");
            int x = int.Parse(Console.ReadLine());
            return x;
        }
    }
}
