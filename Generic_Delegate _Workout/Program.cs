﻿using Generic_Delegate__Workout;
Calculate calculate = new Calculate();
Menu menu = new Menu();
int choice = 0;
do
{
    try
    {
        menu.MenuDisplay();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Console.WriteLine("Result: " + calculate.SumTwoNumber());
                break;
            case 2:
                Console.WriteLine("Result: " + calculate.SubstractTwoNumber());
                break;
            case 3:
                Console.WriteLine("Result: " + calculate.MultiplyTwoNumber());
                break;
            case 4:
                Console.WriteLine("Result: " + calculate.DivideTwoNumber());
                break;
            case 5:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 10");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);