﻿namespace Homework_Predicate
{
    public class Menu
    {
        public void MenuDisplay()
        {
            Console.WriteLine("============Menu============");
            Console.WriteLine("| 1. Add new word          |");
            Console.WriteLine("| 2. Show list word        |");
            Console.WriteLine("| 3. Show list after filter|");
            Console.WriteLine("| 4. Exit                  |");
            Console.WriteLine("============================");
            Console.Write("Choose: ");
        }
    }
}
