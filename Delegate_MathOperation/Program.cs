﻿using Delegate_MathOperation;

Menu menu = new Menu();
Calculate calculator = new Calculate();
int choice = 0;
do
{
	try
	{
        menu.MenuDisplay();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                var sumResult = calculator.SumTwoNumber();
                Console.WriteLine($"Result: {sumResult}");
                break;
            case 2:
                var subResult = calculator.SubstractTwoNumber();
                Console.WriteLine($"Result: {subResult}");
                break;
            case 3:
                var mulResult = calculator.MultiplyTwoNumber();
                Console.WriteLine($"Result: {mulResult}");
                break;
            case 4:
                var divResult = calculator.DivisionTwoNumber();
                Console.WriteLine($"Result: {divResult}");
                break;
            case 5:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose from 1 to 4");
        }
    }
	catch (Exception ex)
	{
        Console.WriteLine(ex.Message);
    }
} while (choice != 5);