﻿namespace Delegate_MathOperation
{
    public class Calculate : Delegate
    {
        private int Addition(int x, int y) => x + y;
        private int Subtraction(int x, int y) => x - y;
        private int Multiplication(int x, int y) => x * y;
        private int Division(int x, int y) => x / y;
        private int FirstNumber;
        private int LastNumber;
        protected override int PerformOperation(int a, int b, MathOperation mathOperation)
        {
            return base.PerformOperation(a, b, mathOperation);
        }

        public int SumTwoNumber()
        {
            Input();
            MathOperation sum = new MathOperation(Addition);
            var result = PerformOperation(FirstNumber, LastNumber, sum);
            return result;
        }
        public int SubstractTwoNumber()
        {
            Input();
            MathOperation subtract = new MathOperation(Subtraction);
            var result = PerformOperation(FirstNumber, LastNumber, subtract);
            return result;
        }
        public int MultiplyTwoNumber()
        {
            Input();
            MathOperation multiply = new MathOperation(Multiplication);
            var result = PerformOperation(FirstNumber, LastNumber, multiply);
            return result;
        }
        public double DivisionTwoNumber()
        {
            Input();
            MathOperation divide = new MathOperation(Division);
            var result = PerformOperation(FirstNumber, LastNumber, divide);
            return result;
        }
        protected void Input()
        {
            Console.Write("Enter x: ");
            FirstNumber = int.Parse(Console.ReadLine());
            Console.Write("Enter y: ");
            LastNumber = int.Parse(Console.ReadLine());
        }
    }
}
