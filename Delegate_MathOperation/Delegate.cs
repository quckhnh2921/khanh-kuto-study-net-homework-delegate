﻿namespace Delegate_MathOperation
{
    public class Delegate
    {
        protected delegate int MathOperation(int a, int b);
        protected virtual int PerformOperation(int a, int b, MathOperation mathOperation) => mathOperation(a, b);
    }
}
